﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Prog_9022РТ018 {
    public abstract class BaseVM : INotifyPropertyChanged {

        public event PropertyChangedEventHandler PropertyChanged;

        protected void notify([CallerMemberName] String propertyName = "") {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
