﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using System.Windows.Media;

namespace Prog_9022РТ018.ViewModel {

    public class mainVM : BaseVM {

        #region properties           
        public logVM log { get; }
        #endregion

        #region commands
        public ButtonClickCommand loadFileCmd { get; }
        public ButtonClickCommand writeCmd { get; }
        public ButtonClickCommand verifyCmd { get; }
        public ButtonClickCommand readCmd { get; }
        #endregion

        public mainVM() : base() {

            log = new logVM();

            loadFileCmd = new ButtonClickCommand(() => {
                log.Info = "Загрузка файла...";

                log.Done = @"Файл C:\firmware900.bin загружен" ;
            });

            writeCmd = new ButtonClickCommand(() => {
            });

            verifyCmd = new ButtonClickCommand(() => {
            });

            readCmd = new ButtonClickCommand(() => {
            });

            //log.Debug = "Отладка";
            //log.Error = "Ошибка";
            //log.Info = "Информация";
            //log.Done = "Выполнено";
            //log.Debug = "Отладка";
            //log.Error = "Ошибка";
            //log.Info = "Информация";
            //log.Done = "Выполнено";

        }

        ~mainVM() {
        }
    }
}
