﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Prog_9022РТ018.ViewModel
{

    public enum type {
        inf,
        err,
        ok,
        dbg
    }

    public enum direction {
        input,
        output
    }

    public class logVM : BaseVM {

        #region properties
        private ObservableCollection<logItem> messages;
        Object lockObj = new Object();
        public ObservableCollection<logItem> Messages {
            get => messages;
            set {
                messages = value;
                notify("Messages");
            }
        }

        private logItem selected;
        public logItem Selected {
            get => selected;
            set {
                selected = value;
                notify("Selected");
            }
        }

        public string Debug {
            //set {
            //    print(value, direction.output, type.dbg);
            //}
            set {
                print(value, direction.output, type.dbg);  
            }
        }

        public string Error {
            set {
                print(value, direction.output, type.err);
            }
        }

        public string Info {
            set {
                print(value, direction.input, type.inf);
            }
        }

        public string Done {
            set {
                print(value, direction.output, type.ok);
            }
        }
        #endregion

        #region commands        
        public ICommand copyCommand { get; }
        public ICommand copyAllCommand { get; }
        public ICommand clearCommand { get; }
        #endregion

        public logVM() {

            copyCommand = new MenuCommand(() => {
                Clipboard.SetText(Selected.Text);
            });

            copyAllCommand = new MenuCommand(() => {
                StringBuilder sb = new StringBuilder();
                foreach (var item in Messages)
                    sb.AppendLine(item.Text);
                Clipboard.SetText(sb.ToString());
            });

            clearCommand = new MenuCommand(() => {                
            });

            Messages = new ObservableCollection<logItem>();
            BindingOperations.EnableCollectionSynchronization(Messages, lockObj);

        }

        #region private     
        void print(string text, direction dir, type typ) {
            Messages.Add(new logItem(text, dir, typ));
        }
        #endregion

        #region public
        public bool showDbg { get; set; }
        #endregion
    }

    public class logItem {

        public string Text { get; set; }                       
        public type Type { get; set; }
        public direction Direction { get; set; }

        public ICommand copyCommand { get; }

        public logItem(string text, direction dir, type typ) {

            Text = text;
            Type = typ;
            Direction = dir;

            copyCommand = new MenuCommand(() => {
                Clipboard.SetText(Text);
            });
        }
    }
}
