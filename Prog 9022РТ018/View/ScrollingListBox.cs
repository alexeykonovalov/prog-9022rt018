﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Prog_9022РТ018.View {
    internal class ScrollingListBox : ListBox {

        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {

            Action action = () => {
                int newItemCount = 0;

                try {
                    newItemCount = e.NewItems.Count;
                }
                catch { }

                if (newItemCount > 0)
                    this.ScrollIntoView(e.NewItems[newItemCount - 1]);

                base.OnItemsChanged(e);
            };

            Dispatcher.BeginInvoke(action);            
        }
    }
}
