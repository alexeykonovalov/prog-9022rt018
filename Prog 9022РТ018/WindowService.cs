﻿using System;
using System.Linq;
using System.Windows;
using System.Resources;
using System.Collections.Generic;
using Prog_9022РТ018.View;
using Prog_9022РТ018.ViewModel;

namespace Prog_9022РТ018 {
    public class WindowService : IWindowService {

        private static WindowService instance;

        List<WindowDialog> windowList = new List<WindowDialog>();

        private WindowService() {

        }

        public static WindowService getInstance() {
            if (instance == null)
                instance = new WindowService();
            return instance;
        }

        public void ShowWindow(BaseVM viewModel) {

            var window = Application.Current
                                 .Windows
                                 .OfType<WindowDialog>()
                                 .FirstOrDefault(x => x.Content?.GetType() == viewModel.GetType());

            if (window == null) {

                if (viewModel is mainVM) {
                    window = new WindowDialog { Content = new mainWnd()};                    
                }

                windowList.Add(window);
                window.Top = 0;
                window.Left = 0;

                try {
                    ResourceSet res = new ResourceSet(@".\" + window.Title + @".resx");

                    //Object w = res.GetObject("Width");
                    //Object h = res.GetObject("Height");
                    Object t = res.GetObject("Top");
                    Object l = res.GetObject("Left");

                    //window.Width = Convert.ToInt32(w);
                    //window.Height = Convert.ToInt32(h);
                    window.Top = Convert.ToInt32(t);
                    window.Left = Convert.ToInt32(l);

                    res.Close();

                }
                catch {
                }

                window.Show();
            }
            else {
                window.Activate();
            }

            window.Closing += Window_Closing;
            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {

            Window window = (Window)sender;
            ResourceWriter writer = new ResourceWriter(@".\" + window.Title + @".resx");

            //writer.AddResource("Height", window.Height);
            //writer.AddResource("Width", window.Width);
            writer.AddResource("Top", window.Top);
            writer.AddResource("Left", window.Left);
            writer.Close();
        }

    }
}
