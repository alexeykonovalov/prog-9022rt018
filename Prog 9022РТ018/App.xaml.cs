﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Prog_9022РТ018.ViewModel;

namespace Prog_9022РТ018 {
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application {

        WindowService ws = WindowService.getInstance();
        mainVM wm = new mainVM();

        protected override void OnStartup(StartupEventArgs e) {

            //wm.showDbg = e.Args.Contains("-debug");
            //wm.showArr = e.Args.Contains("-arr");
            //wm.showCntrlRegs = e.Args.Contains("-enablegodmode");




            ws.ShowWindow(wm);
            base.OnStartup(e);
            
        }

    }
}
